#include <stdint.h>

//! Prepare the OLED screen for use.
void oledInit();

//! Clear the OLED screen.
void oledClear();

//! Print a UTF-8 string to a specific location on the OLED screen.
void oledPrint(int row, int col, const char[]);

/**
 * Print an ASCII-formatted string to the "next" location on the screen.
 *
 * This is intended to be used for on-device debugging purposes, not to be
 * exposed to the user on the other side of the serial connection.
 */
void oledTrace(const char ascii[]);
void oledTrace(uint8_t);
