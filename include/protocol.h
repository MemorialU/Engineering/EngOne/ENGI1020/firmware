#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

#define FULL_VERSION	STR(PROTOCOL_VERSION) "." STR(MINOR_VERSION)


//! Prepare to receive serial commands.
void protoInit();

//! Process the next serial command.
void protoHandleNextCommand();
