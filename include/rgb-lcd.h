//! Clear the RGB LCD display.
void rgbLcdClear();

//! Change the background colour of the RGB LCD display.
void rgbLcdColour(char red, char green, char blue);

//! Write an ASCII string to the RGB LCD display.
void rgbLcdWrite(char row, char col, const char message[]);
