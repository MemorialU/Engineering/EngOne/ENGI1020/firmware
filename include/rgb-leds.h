
//! Change the background colour of the RGB LCD display.
void rgbLedColourRGB(char led, char count, char index, char red, char green, char blue);

//! Write an ASCII string to the RGB LCD display.
void rgbLedColourHSV(char led, char count, char index, float hue, float sat, float val);
