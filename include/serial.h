#include <stdint.h>

//! Prepare the serial port for use.
void serialInit(int baud = 9600);

//! Read a single byte from the serial connection.
uint8_t serialReadByte();

//! Read bytes from the serial connection into a buffer.
int serialReadBytes(char *buffer, int len);

//! Read an Arduino integer (16b) from the serial connection.
int16_t serialReadInt();

//! Read an Arduino unsigned long integer (32b) from the serial connection.
uint32_t serialReadULong();

//! Return a boolean value to the serial connection.
void serialRespond(bool);

//! Return an integer to the serial connection.
void serialRespond(int16_t);

//! Return a string to the serial connection.
void serialRespond(const char message[]);

//! Report an error to the serial connection.
void serialRespondError(const char message[]);
