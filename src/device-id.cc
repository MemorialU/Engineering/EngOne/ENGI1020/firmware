#include <avr/boot.h>

#include <stdio.h>

// Convenience wrapper around the AVR boot signature register(s).
#define BS_BYTE(i) boot_signature_byte_get(i)


const char* deviceString()
{
	static char s[100];
	const char *name = "Grove Beginner Kit";

	if (s[0] == '\0')
	{
		snprintf(s, sizeof(s),
			"%s (%02x %02x %02x %02x %02x %02x %02x %02x)",
			name,
			BS_BYTE(0), BS_BYTE(1), BS_BYTE(2), BS_BYTE(3),
			BS_BYTE(4), BS_BYTE(5), BS_BYTE(6), BS_BYTE(7)
		);
	}

	return s;
}
