#include <DHT.h>

#include "dht11.h"

#define DHTTYPE 11   // DHT 11

static DHT* t_h(char pin)
{
	static DHT t_h(pin, DHTTYPE);
	static bool initialized = false;

	if (not initialized)
	{
		t_h.begin();
		initialized = true;
	}

	return &t_h;
}

float dhtGetTemp(char pin)
{
	float temp_hum_val[2] = {0};
	t_h(pin)->readTempAndHumidity(temp_hum_val);
	return temp_hum_val[1];
}

float dhtGetHumidity(char pin)
{
	float temp_hum_val[2] = {0};
	t_h(pin)->readTempAndHumidity(temp_hum_val);
	return temp_hum_val[0];
}

/*
Add get both
void rgbLcdWrite(char row, char col, const char message[])
{
	lcd()->setCursor(col, row);
	lcd()->print(message);
}*/
