#include <Servo.h>

#include "grove-servo.h"


static Servo* servo(char pin)
{
	static Servo servo;
	static bool initialized = false;

	if (not initialized)
	{
		servo.attach(pin);
		
		initialized = true;
	}

	return &servo;
}

void servoWrite(char pin, char value)
{
	servo(pin)->write(value);
}

int servoRead(char pin)
{
	return (servo(pin)->read());
}