    
    // Function: This program can be used to measure heart rate, the lowest pulse in the program be set to 30.
    //         Use an external interrupt to measure it.
    // Hardware: Grove - Ear-clip Heart Rate Sensor, Grove - Base Shield, Grove - LED
    // Arduino IDE: Arduino-1.0
    // Author: FrankieChu       
    // Date: Jan 22, 2013
    // Version: v1.0
    // by www.seeedstudio.com
    // #define LED 4//indicator, Grove - LED is connected with D4 of Arduino

    #include <Arduino.h>
    #include "heart_rate.h"
    // #include "oled.h"
    
    // bool led_state = false;//state of LED, each time an external interrupt 
    //                                 //will change the state of LED
    unsigned char counter;
    //const int 5 = 5;
    unsigned long temp[11];
    unsigned long sub;
    bool initialized = false;
    bool data_effect=true;
    unsigned int heart_rate = 0;//the measurement result of heart rate
 
    const int max_heartpluse_duty = 2000;//you can change it follow your system's request.
                             //2000 meams 2 seconds. System return error 
                             //if the duty overtrip 2 second.

	
    void heartRateSetup(char pin)
    {
        arrayInit();
        heart_rate = int(pin);
        attachInterrupt(digitalPinToInterrupt(pin), interrupt, RISING);//set interrupt 0,digital port 2
        initialized = true;
    }
    
    unsigned int heartRateReturn(char pin)
    {
        if (initialized == false)
        {
            heartRateSetup(pin);
        }
        return heart_rate;
    }

    //Function: calculate the heart rate
    void sum()
    {
     if(data_effect)
        {
          heart_rate= 600000/(temp[10]-temp[0]);//60*20*1000/20_total_time 
        }
       data_effect=1;//sign bit
    }

    //Function: Interrupt service routine.Get the sigal from the external interrupt
      void interrupt()
    {
        temp[counter]=millis();
        switch(counter)
        {
            case 0:
                sub=temp[counter]-temp[10];
                break;
            default:
                sub=temp[counter]-temp[counter-1];
                break;
        }
        if(sub>max_heartpluse_duty)//set 2 seconds as max heart pluse duty
        {
            data_effect=0;//sign bit
            counter=0;
            arrayInit();
            heart_rate = 0;
        }
        if (counter==10&&data_effect)
        {
            counter=0;
            sum();
        }
        else if(counter!=10&&data_effect)
        counter++;
        else 
        {
            counter=0;
            data_effect=1;
        }
 
    }
    //Function: Initialization for the array(temp)
    void arrayInit()
    {
        for(unsigned char i=0;i < 10;i ++)
        {
            temp[i]=0;
        }
        temp[10]=millis();
    }