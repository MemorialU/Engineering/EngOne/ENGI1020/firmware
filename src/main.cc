#include <Arduino.h>

#include "device-id.h"
#include "oled.h"
#include "protocol.h"

void setup()
{
	oledTrace("Welcome to\nENGI 1020!\n");
	oledTrace("\nFirmware v" FULL_VERSION);

	protoInit();
}

void loop()
{
	protoHandleNextCommand();
}
