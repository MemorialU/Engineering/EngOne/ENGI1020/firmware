#include <U8g2lib.h>

  static U8X8* oled()
 {
 	static U8X8 *oled;

 	if (not oled)
 	{
 		oled = new U8X8_SSD1306_128X64_NONAME_HW_I2C(U8X8_PIN_NONE);

 		oled->begin();
 		oled->setFlipMode(1);
 		oled->setFont(u8x8_font_amstrad_cpc_extended_f);
 		oled->setCursor(0, 0);
 	}

 	return oled;
 }

 void oledClear()
 {
 	oled()->clearDisplay();
 }

 void oledPrint(int row, int col, const char message[])
 {
 	oled()->drawUTF8(col, 2 * row, message);
 }

 //void oledPrint(int row, int col, const char message[])
 //{
 //	oled()->drawUTF8(col, 2 * row, message);
 //}

 void oledTrace(const char ascii[])
 {
 	oled()->print(ascii);
 }

 void oledTrace(byte b)
 {
 	oled()->print(b);
 }
