#include <Seeed_BMP280.h>
#include <Wire.h>

#include "pressure.h"

uint32_t MSL = 102009; //Mean sea level air pressure in Pa

static BMP280* pressure()
{
	static BMP280 pressure;
	static bool initialized = false;

	if (not initialized)
	{
		pressure.init();
		
		initialized = true;
	}

	return &pressure;
}

float getPressure()
{
	return pressure()->getPressure();
}

float getTemperature()
{
	return pressure()->getTemperature();
}

float getAltitude()
{
	return pressure()->calcAltitude(102009);
}