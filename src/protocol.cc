#include <Arduino.h>

#include "device-id.h"
#include "oled.h"
#include "protocol.h"
#include "rgb-lcd.h"
#include "serial.h"
#include "dht11.h"
#include "three-axis-accel.h"
#include "pressure.h"
#include "grove-servo.h"
#include "ultra-distance.h"
#include "rgb-leds.h"
#include "heart_rate.h"

static void dispatch(char);
static void dispatchAnalog();
static void dispatchDigital();
static void dispatchOLED();
static void dispatchRgbLcd();
static void dispatchBuzzer();
static void dispatchDHT();
static void dispatch3AxisAccel();
static void dispatchPressure();
static void dispatchServo();
static void dispatchUltraDistance();
static void dispatchHeartRate();
static void dispatchRgbLeds();

void protoInit()
{
	char buf[64];
	sprintf(buf, "ENGI 1020 v%d on %s\n", PROTOCOL_VERSION, deviceString());
	serialInit();
	serialRespond(buf);
}

void protoHandleNextCommand()
{
	// Pause for 1/200 s between commands.
	//
	// Why this magic constant, you ask? Because... magic!
	// Seriously, this seems to be a limitation of Arduino's serial
	// implementation, which uses three wires and thus no flow control.
	delay(50);

	char b = serialReadByte();
	dispatch(b);
}

void dispatch(char b)
{
	switch (b)
	{
	case 'a':
		dispatchAnalog();
		break;

	case 'd':
		dispatchDigital();
		break;

	case 'o':
		dispatchOLED();
		break;

	case 'R':
		dispatchRgbLcd();
		break;

	case 'b':
		dispatchBuzzer();
		break;

	case 'D':
		dispatchDHT();
		break;

	case '3':
		dispatch3AxisAccel();
		break;

	case 'p':
		dispatchPressure();
		break;

	case 's':
		dispatchServo();
		break;

	case 'u':
		dispatchUltraDistance();
		break;

	case 'l':
		dispatchRgbLeds();
		break;

	case 'h':
	 	dispatchHeartRate();
	 	break;

	default:
		char buf[64];
		sprintf(buf, "Unknown command: '%c'", b);
		serialRespondError(buf);
	}
}

void dispatchAnalog()
{
	char op = serialReadByte();
	char pin;
	int value;

	switch (op)
	{
	case 'r':
		pin = serialReadByte();
		value = analogRead(pin);
		serialRespond(value);
		break;

	case 'w':
		pin = serialReadByte();
		value = serialReadByte();
		analogWrite(pin, value);
		serialRespond(("AW-OK"));
		break;

	default:
		char buf[64];
		sprintf(buf, "Unknown analog operation: '%c'", op);
		serialRespondError(buf);
	}
}

void dispatchDigital()
{
	char op = serialReadByte();
	char pin;
	bool value;

	switch (op)
	{
	case 'r':
		pin = serialReadByte();
		value = digitalRead(pin);
		serialRespond(value);
		break;

	case 'w':
		pin = serialReadByte();
		value = serialReadByte();
		pinMode(pin, OUTPUT);
		digitalWrite(pin, value);
		serialRespond(("DW-OK"));
		break;

	default:
		char buf[64];
		sprintf(buf, "Unknown digital operation: '%c'", op);
		serialRespondError(buf);
	}
}

void dispatchOLED()
{
	char op = serialReadByte();

	switch (op)
	{
	case 'c':
		oledClear();
		serialRespond(("OC-OK"));
		break;

	case 'p':
	{
		char row = serialReadByte();
		char col = serialReadByte();
		size_t len = serialReadByte();

		char buf[len + 1];
		serialReadBytes(buf, len);
		buf[len] = '\0';

		oledPrint(row, col, buf);

		serialRespond(("OP-OK"));
		break;
	}

	default:
		char buf[64];
		sprintf(buf, "Unknown OLED operation: '%c'", op);
		serialRespondError(buf);
	}
}

void dispatchRgbLcd()
{
	char op = serialReadByte();

	switch (op)
	{
	case 'c':
		rgbLcdClear();
		serialRespond("Rc-OK");
		break;

	case 'C':
	{
		char r = serialReadByte();
		char g = serialReadByte();
		char b = serialReadByte();

		rgbLcdColour(r, g, b);
		serialRespond(("RC-OK"));
		break;

	}

	case 'p':
	{
		char row = serialReadByte();
		char col = serialReadByte();
		size_t len = serialReadByte();

		char buf[len + 1];
		serialReadBytes(buf, len);
		buf[len] = '\0';

		rgbLcdWrite(row, col, buf);
		serialRespond(("Rp-OK"));
		break;

	}

	default:
		char buf[64];
		sprintf(buf, "Unknown RGB LCD operation: '%c'", op);
		serialRespondError(buf);
	}
}

void dispatchBuzzer()
{
	char op = serialReadByte();
	char pin;
	char freq_upper;
	char freq_lower;
	int freq;

	switch (op)
	{
	case 'f':
		
		//Play note indefinitely
		pin = serialReadByte();
		freq_lower = serialReadByte();
		freq_upper = serialReadByte();
		freq = (freq_upper << 8) + freq_lower ;
		tone(pin, freq);
		serialRespond(("BF-OK"));
		break;

	case 't':
		int sec;
		char sec_first, sec_second, sec_third, sec_fourth;
		//Play note for given time
		pin = serialReadByte();
		freq_lower = serialReadByte();
		freq_upper = serialReadByte();
		freq = (freq_upper << 8) + freq_lower ;
		
		sec_first = serialReadByte();
		sec_second = serialReadByte();
		sec_third = serialReadByte();
		sec_fourth = serialReadByte();

		sec = (sec_fourth << 8) + sec_third;
		sec = (sec << 8) + sec_second;
		sec = (sec << 8) + sec_first;
		tone(pin, freq, sec);
		serialRespond(("BT-OK"));
		break;
	
	case 's':
		//Stop buzzer
		pin = serialReadByte();
		noTone(pin);
		serialRespond(("BS-OK"));
		break;

	default:
		char buf[64];
		sprintf(buf, "Unknown buzzer operation: '%c'", op);
		serialRespondError(buf);
	}
}

void dispatchDHT()
{
	char op = serialReadByte();
	char pin;
	float temp;
	float humidity;
	
	switch(op)
	{
	case 't':
		// Get temp
		pin = serialReadByte();
		temp = dhtGetTemp(pin);
		serialRespond((int)(temp*100));
		break;

	case 'h':
		//Get humidity
		pin = serialReadByte();
		humidity = dhtGetHumidity(pin);
		serialRespond((int)(humidity*100));
		break;

	default:
		char buf[64];
		sprintf(buf, "Unknown temp-humidity operation: '%c'", op);
		serialRespondError(buf);	
	}


}

void dispatch3AxisAccel()
{
	char op = serialReadByte();
	float x;
	float y;
	float z;
	
	switch(op)
	{
	case 'x':
		// Get x-axis acceleration
		x = getAccelerationX();
		serialRespond((int)(x*100));
		break;

	case 'y':
		// Get y-axis acceleration
		y = getAccelerationY();
		serialRespond((int)(y*100));
		break;

	case 'z':
		// Get z-axis acceleration
		z = getAccelerationZ();
		serialRespond((int)(z*100));
		break;

	default:
		char buf[64];
		sprintf(buf, "Unknown accelerometer operation: '%c'", op);
		serialRespondError(buf);	
	}
}

void dispatchPressure()
{
	char op = serialReadByte();
	float t;
	float p;
	float a;
	
	switch(op)
	{
	case 't':
		// Get temperature from BMP280 in C
		t = getTemperature();
		serialRespond((int)(t*100)); //TODO - Figure out scaling
		break;

	case 'p':
		// Get pressure from BMP280 in Pa
		p = getPressure();
		serialRespond((int)(p*100)); //TODO - Figure out scaling
		break;

	case 'a':
		// Get altitude from BMP280 in m
		a = getAltitude();
		serialRespond((int)(a*100)); //TODO - Figure out scaling
		break;

	default:
		char buf[64];
		sprintf(buf, "Unknown pressure operation: '%c'", op);
		serialRespondError(buf);	
	}
}

void dispatchServo()
{
	char op = serialReadByte();
	char pin;
	int inputAngle;
	int outputAngle;
	
	switch(op)
	{
	case 'w':
		// Set servo angle
		pin = serialReadByte();
		inputAngle = serialReadByte();
		servoWrite(pin, inputAngle);
		serialRespond(("SW-OK")); 
		break;

	case 'r':
		// Get servo angle
		pin = serialReadByte();
		outputAngle = servoRead(pin);
		serialRespond(outputAngle);
		break;

	default:
		char buf[64];
		sprintf(buf, "Unknown servo operation: '%c'", op);
		serialRespondError(buf);	
	}
}

void dispatchUltraDistance()
{
	char op = serialReadByte();
	char pin;
	float cm;
	float inches;
	
	switch(op)
	{
	case 'c':
		// Get distance in cm
		pin = serialReadByte();
		cm = ultraGetCentimeters(pin);
		serialRespond((int)(cm*100));
		break;

	case 'i':
		// Get distance in inches
		pin = serialReadByte();
		cm = ultraGetInches(pin);
		serialRespond((int)(inches*100));
		break;

	default:
		char buf[64];
		sprintf(buf, "Unknown ultrasonic distance operation: '%c'", op);
		serialRespondError(buf);	
	}
}

void dispatchHeartRate()
{
	char op = serialReadByte();
	char pin;
 	char rate;

	switch(op)
	{
		case 's':
			pin = serialReadByte();
			heartRateSetup(pin);
			serialRespond(("HS-OK")); 
			break;

		case 'r':
			pin = serialReadByte();
			rate = heartRateReturn(pin);
			serialRespond(rate);
			break;

		default:
			char buf[64];
			sprintf(buf, "Unknown heart monitor operation: '%c'", op);
			serialRespondError(buf);	
	}


}

void dispatchRgbLeds()
{
	char op = serialReadByte();
	char pin;
	char count;
	char index;
	char red;
	char green;
	char blue;
	float hue;
	float sat;
	float val;
	
	switch(op)
	{
	case 'r':
		// Set colour in RGB
		pin = serialReadByte();
		count = serialReadByte();
		index = serialReadByte();
		red = serialReadByte();
		green = serialReadByte();
		blue = serialReadByte();
		rgbLedColourRGB(pin, count, index, red, green, blue);
		serialRespond("lC-OK");
		break;

	case 'h':
		pin = serialReadByte();
		count = serialReadByte();
		index = serialReadByte();
		hue = float(serialReadByte())/255.0;
		sat = float(serialReadByte())/255.0;
		val = float(serialReadByte())/255.0;
		rgbLedColourHSV(pin, count, index, hue, sat, val);
		serialRespond("lH-OK");
		break;

	default:
		char buf[64];
		sprintf(buf, "Unknown RGB LED operation: '%c'", op);
		serialRespondError(buf);	
	}
}

