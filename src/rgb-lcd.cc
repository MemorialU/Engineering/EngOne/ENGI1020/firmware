#include <rgb_lcd.h>

#include "rgb-lcd.h"


static rgb_lcd* lcd()
{
	static rgb_lcd lcd;
	static bool initialized = false;

	if (not initialized)
	{
		lcd.begin(16, 2);
		initialized = true;
	}

	return &lcd;
}


void rgbLcdClear()
{
	lcd()->clear();
}

void rgbLcdColour(char red, char green, char blue)
{
	lcd()->setRGB(red, green, blue);
}

void rgbLcdWrite(char row, char col, const char message[])
{
	lcd()->setCursor(col, row);
	lcd()->print(message);
}
