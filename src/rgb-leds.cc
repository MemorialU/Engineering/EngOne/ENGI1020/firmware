#include <ChainableLED.h>

#include "rgb-leds.h"


static ChainableLED* leds(char pin, char count)
{
	static bool initialized = false;
	static ChainableLED leds(pin, pin+1, count);

	if (not initialized)
	{
		initialized = true;
	}

	return &leds;
}


void rgbLedColourRGB(char pin, char count, char index, char red, char green, char blue)
{
	leds(pin, count)->setColorRGB(index, red, green, blue);
}

void rgbLedColourHSV(char pin, char count, char index, float hue, float sat, float val)
{
	leds(pin, count)->setColorHSB(index, hue, sat, val);
}
