#include <Arduino.h>
#include <U8g2lib.h>

#include "oled.h"
#include "serial.h"


//! Write a TLV-encoded value to the serial connection.
static void sendTLV(char ty, uint8_t len, const char *value);

void serialInit(int baud)
{
	Serial.begin(baud, SERIAL_8N1);
}

uint8_t serialReadByte()
{
	while (true)
	{
		char buf[2] = {0, 0};
		int len = Serial.readBytes(buf, 1);
		if (len > 0)
		{
			return buf[0];
		}

		delay(10);
	}
}

int serialReadBytes(char *buffer, int len)
{
	return Serial.readBytes(buffer, len);
}

int16_t serialReadInt()
{
	char buf[2];
	Serial.readBytes(buf, sizeof(buf));

	return 256 * buf[0] + buf[1];
}

uint32_t serialReadULong()
{
	char buf[4];
	Serial.readBytes(buf, sizeof(buf));

	uint32_t value = 0;

	value += buf[0];
	value <<= 8;
	value += buf[1];
	value <<= 8;
	value += buf[2];
	value <<= 8;
	value += buf[3];

	return value;
}

void serialRespond(bool b)
{
	sendTLV('B', 1, reinterpret_cast<char *>(&b));
}

void serialRespond(int16_t n)
{
	sendTLV('I', sizeof(n), reinterpret_cast<char *>(&n));
}

void serialRespond(const char message[])
{
	sendTLV('S', strlen(message), message);
}

void serialRespondError(const char message[])
{
	sendTLV('E', strlen(message), message);
}

void sendTLV(char ty, uint8_t len, const char *value)
{
	Serial.write(ty);
	Serial.write(len);
	Serial.write(value, len);
}
