#include <LIS3DHTR.h>
#include <Wire.h>
#define WIRE Wire

#include "three-axis-accel.h"


static LIS3DHTR<TwoWire>* accel()
{
	static LIS3DHTR<TwoWire> accel;
	static bool initialized = false;

	if (not initialized)
	{
		accel.begin(WIRE, 0x19);
		accel.setOutputDataRate(LIS3DHTR_DATARATE_50HZ);
		accel.setHighSolution(true); //High solution enable

		initialized = true;
	}

	return &accel;
}

float getAccelerationX()
{
	return (accel()->getAccelerationX());
}

float getAccelerationY()
{
	return (accel()->getAccelerationY());
}

float getAccelerationZ()
{
	return (accel()->getAccelerationZ());
}