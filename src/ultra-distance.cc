#include "ultra-distance.h"


static Ultrasonic* u_d(char pin)
{
	static Ultrasonic u_d(pin);
	static bool initialized = false;

	if (not initialized)
	{
		initialized = true;
	}

	return &u_d;
}

float ultraGetCentimeters(char pin)
{
	return u_d(pin)->MeasureInCentimeters();
}

float ultraGetInches(char pin)
{
	return u_d(pin)->MeasureInInches();
}
